package me.sevant.kpi.items

import org.bukkit.Material
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.ItemMeta

fun Material.asItemStack(amount: Int = 1): ItemStack = ItemStack(this, amount)

fun ItemStack.meta(builder: ItemMeta.() -> Unit) {
    itemMeta = this.itemMeta?.apply(builder)
}

fun ItemStack.builder(builder: ItemStack.() -> Unit): ItemStack {
    return this.apply(builder)
}

inline fun item(builder: ItemData.() -> Unit): ItemStack {
    val data = ItemData().apply(builder)
    val item = ItemStack(data.material, data.amount)
    item.itemMeta = item.itemMeta?.apply(data.metaBuilder)
    return item
}

data class ItemData(
    var material: Material = Material.AIR,
    var amount: Int = 1,
    var metaBuilder: ItemMeta.() -> Unit = {}
)