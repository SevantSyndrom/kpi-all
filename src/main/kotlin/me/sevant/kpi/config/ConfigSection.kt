package me.sevant.kpi.config

import com.google.common.reflect.TypeToken.*
import ninja.leaping.configurate.ConfigurationNode
import ninja.leaping.configurate.loader.ConfigurationLoader

open class ConfigSection {

    var node: ConfigurationNode? = null
    var loader: ConfigurationLoader<*>? = null

    fun save() {
        if (node == null) return

        loader?.save(node!!)
    }

    fun get(path: String, def: Any? = null): Any? {
        return node?.getNode(parsePath(path))?.value ?: def
    }

    @Suppress("UNCHECKED_CAST")
    @JvmName("getGeneric")
    fun <T> get(path: String, default: T? = null): T? {
        return this.get(path, default) as T
    }
    fun getBool(path: String, default: Boolean? = null): Boolean? {
        return node?.getNode(parsePath(path))?.boolean ?: default
    }
    fun getString(path: String, default: String? = null): String? {
        return node?.getNode(parsePath(path))?.string ?: default
    }

    fun getInt(path: String, default: Int? = null): Int? {
        return node?.getNode(parsePath(path))?.int ?: default
    }

    fun getFloat(path: String, default: Float? = null): Float? {
        return node?.getNode(parsePath(path))?.float ?: default
    }

    fun getDouble(path: String, default: Double? = null): Double? {
        return node?.getNode(parsePath(path))?.double ?: default
    }

    fun getLong(path: String, default: Long? = null): Long? {
        return node?.getNode(parsePath(path))?.long ?: default
    }

    @Suppress("UnstableApiUsage")
    inline fun <reified T> getListOf(path: String, default: List<T>? = null): List<T>? {
        return node?.getNode(parsePath(path))?.getList(of(T::class.java)) ?: default
    }

    fun getBoolList(path: String, default: List<Boolean>? = null): List<Boolean>? {
        return this.getListOf<Boolean>(path, default)
    }

    fun getStringList(path: String, default: List<String>? = null): List<String>? {
        return this.getListOf<String>(path, default)
    }

    fun getIntList(path: String, default: List<Int>? = null): List<Int>? {
        return this.getListOf<Int>(path, default)
    }

    fun getFloatList(path: String, default: List<Float>? = null): List<Float>? {
        return this.getListOf<Float>(path, default)
    }

    fun getDoubleList(path: String, default: List<Double>? = null): List<Double>? {
        return this.getListOf<Double>(path, default)
    }

    fun getLongList(path: String, default: List<Long>? = null): List<Long>? {
        return this.getListOf<Long>(path, default)
    }

    fun getConfigSection(path: String): ConfigSection {
        val c = ConfigSection()
        c.node = node?.getNode(parsePath(path))
        return c
    }

    fun set(path: String, value: Any) {
        node?.getNode(parsePath(path))?.value = value
    }

    fun keys(): List<String> {
        val list = mutableListOf<String>()
        this.node?.childrenMap?.keys?.forEach {
            list.add(it as String)
        }
        return list
    }

    fun isEmpty(): Boolean {
        return this.keys().isEmpty()
    }

    fun hasKey(key: String): Boolean {
        return this.keys().contains(key)
    }

    fun parsePath(path: String): Array<Any?>? {
        var ar = arrayOfNulls<Any>(1)
        if (path.contains(".")) {
            val split = path.split("\\.").toTypedArray()
            ar = arrayOfNulls(split.size)
            for (i in split.indices) {
                ar[i] = split[i] as Any
            }
        } else {
            ar[0] = path as Any
        }
        return ar
    }
}