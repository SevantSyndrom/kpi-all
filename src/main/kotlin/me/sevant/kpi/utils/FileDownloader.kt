package me.sevant.kpi.utils


import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.*
import java.net.URL

class FileDownloader(val url : URL) {

    fun download(file : File){
        GlobalScope.async {
            val connection = url.openConnection()
            connection.connectTimeout = 10_000
            connection.readTimeout = 10_000
            val tempFile = file.parentFile.resolve("${file.name}.temp")
            val reader = BufferedReader(InputStreamReader(connection.getInputStream()))
            val writer = BufferedWriter(FileWriter(tempFile))
            val lines = reader.readLines()
            for(line in lines){
                writer.write(line)
                writer.write("\n")
            }
            reader.close()
            writer.close()

            tempFile.copyTo(file)
            tempFile.delete()


        }




    }

}