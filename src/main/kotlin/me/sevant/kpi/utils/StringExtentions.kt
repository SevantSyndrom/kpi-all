package me.sevant.kpi.utils

fun String.colorize(): String {
    return this.replace("&", "§")
}