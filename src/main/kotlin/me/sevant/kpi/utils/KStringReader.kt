package me.sevant.kpi.utils

class KStringReader(private val data: String) {

    var cursor: Int = 0

    constructor(other: KStringReader) : this(other.data) {
        cursor = other.cursor
    }

    fun canRead(length: Int): Boolean {
        return cursor + length <= data.length
    }

    fun canRead(): Boolean {
        return canRead(1)
    }

    fun getRead(): String {
        return data.substring(0, cursor)
    }

    fun getString(): String {
        return data
    }

    fun getTotalLength(): Int {
        return data.length
    }

    fun peek(): Char {
        return data[cursor]
    }

    fun peek(off: Int): Char {
        return data[cursor + off]
    }

    fun getRemaining(): String {
        return data.substring(cursor, data.length)
    }

    fun getRemainingLength(): Int {
        return data.length - cursor
    }

    fun read(): Char {
        return data[cursor++]
    }

    private fun isNumericallyValid(char: Char): Boolean {
        return char in '0'..'9' || char == '.' || char == '-'
    }

    fun skip() {
        cursor++
    }

    fun skipWhiteSpace() {
        while (canRead() && Character.isWhitespace(peek())) {
            skip()
        }
    }

    fun readInt(): Int {
        val start: Int = cursor
        while (canRead() && isNumericallyValid(peek())) {
            skip()
        }

        val num = data.substring(start, cursor)
        if (num.isEmpty()) {
            throw(StringParseException("Attempted to parse an empty string to integer"))
        }

        try {
            return num.toInt()
        } catch (ex: NumberFormatException) {
            cursor = start
            throw (StringParseException("Attempted to parse $num as an integer!"))
        }
    }

    fun readLong(): Long {
        val start: Int = cursor
        while (canRead() && isNumericallyValid(peek())) {
            skip()
        }

        val num = data.substring(start, cursor)
        if (num.isEmpty()) {
            throw(StringParseException("Attempted to parse an empty string to a Long"))
        }

        try {
            return num.toLong()
        } catch (ex: NumberFormatException) {
            cursor = start
            throw(StringParseException("Attempted to parse $num to a Long"))
        }
    }

    fun readDouble(): Double {
        val start: Int = cursor
        while (canRead() && isNumericallyValid(peek())) {
            skip()
        }

        val num = data.substring(start, cursor)

        if (num.isEmpty()) {
            throw (StringParseException("Attempted to parse an empty string to a Double"))
        }

        try {
            return num.toDouble()
        } catch (ex: NumberFormatException) {
            cursor = start
            throw (StringParseException("Attempted to parse $num to a Double"))
        }
    }

    fun readFloat(): Float {
        val start: Int = cursor
        while (canRead() && isNumericallyValid(peek())) {
            skip()
        }

        val num = data.substring(start, cursor)

        if (num.isEmpty()) {
            throw(StringParseException("Attempted to parse an empty string to a Float"))
        }

        try {
            return num.toFloat()
        } catch (ex: NumberFormatException) {
            cursor = start
            throw(StringParseException("Attempted to parse $num as a Float"))
        }
    }

    fun readString(): String {
        return readUntil(' ')
    }

    fun readUntil(delimiter: Char): String {

        val result = StringBuilder()
        var escaped = false
        while (canRead()) {
            val c = read()

            if (c == delimiter) {
                return result.toString()
            } else {
                result.append(c)
            }
        }

        throw(NoStringToParseException("No string to parse"))
    }

    private val SYNTAX_ESCAPE = '\\'
    private val SYNTAX_DOUBLE_QUOTE = '"'
    private val SYNTAX_SINGLE_QUOTE = '\''
}

class StringParseException(val msg: String) : Exception()

class NoStringToParseException(val msg: String) : Exception()