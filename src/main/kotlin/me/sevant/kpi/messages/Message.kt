package me.sevant.kpi.messages

import me.sevant.kpi.messages.Action.*
import net.md_5.bungee.api.chat.*
import org.bukkit.ChatColor
import org.bukkit.entity.Player

class Message {
    private val tc = ComponentBuilder("")

    fun content(builder: MessageContent.() -> Unit) {
        tc.append(MessageContent().apply(builder).build())
    }

    fun newLine() {
        tc.append("\n")
    }

    fun build(): Array<BaseComponent> {
        return tc.create()
    }
}

class MessageContent(var text: String = "") {
    val actions = hashSetOf<ActionContent>()
    fun build(): TextComponent {
        val tc = TextComponent(ChatColor.translateAlternateColorCodes('&', text))
        for (content in actions) {
            when (content.action) {

                HOVER_SHOW_TEXT -> tc.hoverEvent =
                    HoverEvent(HoverEvent.Action.SHOW_TEXT, ComponentBuilder(content.data as String).create())

                HOVER_SHOW_ITEM -> TODO("Not yet implemented!")

                HOVER_SHOW_ADVANCEMENT -> TODO("Not yet implemented!")

                HOVER_SHOW_ENTITY -> TODO("Not yet implemented!")

                CLICK_CHANGE_PAGE -> TODO("Not yet implemented!")

                CLICK_OPEN_FILE -> TODO("Not yet implemented!")

                CLICK_SUGGEST_COMMAND -> tc.clickEvent =
                    ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, content.data as String)

                CLICK_RUN_COMMAND -> tc.clickEvent =
                    ClickEvent(ClickEvent.Action.RUN_COMMAND, content.data as String)

                CLICK_OPEN_URL -> tc.clickEvent = ClickEvent(ClickEvent.Action.OPEN_URL, content.data as String)
            }
        }
        return tc
    }
}

fun Any.message(builder: Message.() -> Unit) = Message().apply(builder)

fun Player.sendMessage(message: Message) {
    this.spigot().sendMessage(*message.build())
}