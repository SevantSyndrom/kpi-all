package me.sevant.kpi.command

import me.sevant.kpi.messages.*
import me.sevant.kpi.utils.colorize
import java.lang.StringBuilder
import java.util.*
import kotlin.collections.LinkedHashMap

open class CommandFormatter(private val root: Command) {

    val message: LinkedHashMap<Message, String> by lazy {
        buildOutMessage(buildPaths())
    }

    open fun buildOutMessage(paths: LinkedHashMap<Command, List<String>>): LinkedHashMap<Message, String> {
        val msg = linkedMapOf<Message, String>()
        val mess = message {
            content {
                text = "&3&l|-------[ ${root.label.toUpperCase()} HELP ]-------|"
            }
            newLine()
            content {
                text = "&aRequired = &l[] &5Optional = &l<>"
            }
            newLine()
        }
        msg[mess] = ""
        for (entry in paths) {
            val m = message {

                content {
                    val builder = StringBuilder("&c/")
                    for (s in entry.value) {
                        builder.append(s)
                        builder.append(" ")
                    }
                    text = builder.toString()
                    withAction(Action.HOVER_SHOW_TEXT) {
                        """
                            &aDescription: &5${entry.key.description}
                        """.trimIndent().colorize()
                    }
                }

                entry.key.arguments.forEach { key, argument ->
                    content {
                        text = if (argument.required) {
                            "&a[$key] "
                        } else {
                            "&5<$key> "
                        }

                        if (argument.description.isNotEmpty()) {
                            withAction(Action.HOVER_SHOW_TEXT) {
                                    """
                                    &aDescription: &5${argument.description}
                                    """.trimIndent().colorize()
                            }
                        }
                    }
                }
            }
            msg[m] = entry.key.permission
        }

        return msg
    }

    private fun buildPaths(): LinkedHashMap<Command, List<String>> {
        val paths = linkedMapOf<Command, List<String>>()

        val queue = LinkedList<Command>()
        queue.add(root)
        while (queue.isNotEmpty()) {
            val command = queue.poll()
            val list = mutableListOf<String>()
            var c = command
            list.add(c.label)
            while (c.hasParent()) {
                c = c.parent
                list.add(c.label)
            }
            paths[command] = list.reversed()

            command.children.forEach {
                queue.add(it.value)
            }
        }

        return paths
    }
}
