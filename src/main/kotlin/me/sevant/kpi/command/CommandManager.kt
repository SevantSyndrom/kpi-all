package me.sevant.kpi.command

import org.bukkit.Bukkit
import org.bukkit.command.CommandMap
import org.bukkit.plugin.Plugin
import org.bukkit.plugin.SimplePluginManager

class CommandManager(private val plugin: Plugin) {

    val registered = hashSetOf<Command>()

    fun register(command: Command) {
        val c = CommandExecutor(command)
        registered.add(command)
        commandMap.register(plugin.name, c)
    }

    companion object {
        val commandMap: CommandMap by lazy {
            val f = SimplePluginManager::class.java.getDeclaredField("commandMap")
            f.isAccessible = true
            f.get(Bukkit.getPluginManager()) as CommandMap
        }
    }
}
