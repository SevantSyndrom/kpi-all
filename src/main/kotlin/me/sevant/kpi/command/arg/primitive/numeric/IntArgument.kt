package me.sevant.kpi.command.arg.primitive.numeric

import me.sevant.kpi.command.ArgumentData
import me.sevant.kpi.command.CommandData
import me.sevant.kpi.command.arg.ArgumentType
import me.sevant.kpi.command.arg.CommandSyntaxException
import me.sevant.kpi.utils.KStringReader

class IntArgument(val min: Int = Int.MIN_VALUE, val max: Int = Int.MAX_VALUE) :
    ArgumentType<Int> {

    override fun parse(reader: KStringReader): Int {
        val start = reader.cursor
        val res = reader.readInt()

        if (res < min) {
            reader.cursor = start
            throw CommandSyntaxException("Argument was less than the minimum. Found : $res Minimum: $min")
        }

        if (res > max) {
            reader.cursor = start
            throw CommandSyntaxException("Argument was more than the maximum. Found : $res Maximum: $max")
        }

        return res
    }
}

fun CommandData.int(arg: ArgumentData) = arg.data as Int