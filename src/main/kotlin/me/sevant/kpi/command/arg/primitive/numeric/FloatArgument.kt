package me.sevant.kpi.command.arg.primitive.numeric

import me.sevant.kpi.command.ArgumentData
import me.sevant.kpi.command.CommandData
import me.sevant.kpi.command.arg.ArgumentType
import me.sevant.kpi.command.arg.CommandSyntaxException
import me.sevant.kpi.utils.KStringReader

class FloatArgument(val min: Float = Float.MAX_VALUE, val max: Float = Float.MAX_VALUE) :
    ArgumentType<Float> {

    override fun parse(reader: KStringReader): Float {
        val start = reader.cursor
        val res = reader.readFloat()
        if (res < min) {
            reader.cursor = start
            throw CommandSyntaxException("Argument was less than the minimum. Found : $res Minimum: $min")
        }
        if (res > max) {
            reader.cursor = start
            throw CommandSyntaxException("Argument was more than the maximum. Found : $res Maximum: $max")
        }
        return res
    }
}

fun CommandData.float(arg: ArgumentData) = arg.data as Float