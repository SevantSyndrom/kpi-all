package me.sevant.kpi.command.arg.minecraft

import me.sevant.kpi.command.ArgumentData
import me.sevant.kpi.command.CommandData
import me.sevant.kpi.command.arg.ArgumentType
import me.sevant.kpi.command.arg.CommandSyntaxException
import me.sevant.kpi.utils.KStringReader
import org.bukkit.Bukkit
import org.bukkit.OfflinePlayer

class OfflinePlayerArgument : ArgumentType<OfflinePlayer> {

    @Suppress("DEPRECATION")
    override fun parse(reader: KStringReader): OfflinePlayer {
        val start = reader.cursor
        val playerName = reader.readString()
        val player = Bukkit.getOfflinePlayer(playerName)
        if (player.name != playerName) {
            reader.cursor = start
            throw (CommandSyntaxException("Failed to get $playerName as an offline player. Maybe they have never joined?"))
        }
        return player
    }
}

fun CommandData.offlinePlayer(arg: ArgumentData) = arg.data as OfflinePlayer
