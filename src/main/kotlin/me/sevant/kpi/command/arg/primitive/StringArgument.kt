package me.sevant.kpi.command.arg.primitive

import me.sevant.kpi.command.ArgumentData
import me.sevant.kpi.command.CommandData
import me.sevant.kpi.command.arg.ArgumentType
import me.sevant.kpi.utils.KStringReader

class StringArgument : ArgumentType<String> {

    override fun parse(reader: KStringReader): String {
        return reader.getString()
    }
}

fun CommandData.string(arg: ArgumentData) = arg.data as String