package me.sevant.kpi.command.arg.primitive.numeric

import me.sevant.kpi.command.ArgumentData
import me.sevant.kpi.command.CommandData
import me.sevant.kpi.command.arg.ArgumentType
import me.sevant.kpi.command.arg.CommandSyntaxException
import me.sevant.kpi.utils.KStringReader

class DoubleArgument(val min: Double = Double.MIN_VALUE, val max: Double = Double.MAX_VALUE) :
    ArgumentType<Double> {

    override fun parse(reader: KStringReader): Double {
        val start = reader.cursor
        val res = reader.readDouble()

        if (res < min) {
            reader.cursor = start
            throw CommandSyntaxException("Argument was less than the minimum. Found : $res Minimum: $min")
        }
        if (res > max) {
            reader.cursor = start
            throw CommandSyntaxException("Argument was more than the maximum. Found : $res Maximum: $max")
        }
        return res
    }
}

fun CommandData.double(arg: ArgumentData) = arg.data as Double