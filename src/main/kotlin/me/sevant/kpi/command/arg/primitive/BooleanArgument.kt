package me.sevant.kpi.command.arg.primitive

import me.sevant.kpi.command.ArgumentData
import me.sevant.kpi.command.CommandData
import me.sevant.kpi.command.arg.ArgumentType
import me.sevant.kpi.command.arg.CommandSyntaxException
import me.sevant.kpi.utils.KStringReader

class BooleanArgument : ArgumentType<Boolean> {

    override fun parse(reader: KStringReader): Boolean {
        val start = reader.cursor
        val boolString = reader.readString().toLowerCase()
        if (boolString == "true" || boolString == "t" || boolString == "1") {
            return true
        } else if (boolString == "false" || boolString == "f" || boolString == "0") {
            return false
        } else {
            reader.cursor = start
            throw(CommandSyntaxException("Could not parse $boolString as a boolean! Try true or false instead."))
        }
    }
}

fun CommandData.bool(arg: ArgumentData) = arg.data as Boolean