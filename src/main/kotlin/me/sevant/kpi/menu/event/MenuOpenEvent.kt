package me.sevant.kpi.menu.event

import me.sevant.kpi.menu.Menu
import org.bukkit.entity.Player
import org.bukkit.event.Cancellable

class MenuOpenEvent(player: Player, menu: Menu) : MenuEvent(player, menu), Cancellable {

    private var c = false
    override fun setCancelled(bool: Boolean) {
        c = bool
    }

    override fun isCancelled(): Boolean {
        return c
    }
}