package me.sevant.kpi.menu.event

import org.bukkit.entity.Player
import org.bukkit.event.Event
import org.bukkit.event.HandlerList

open class PlayerEvent(val player: Player) : Event() {
    override fun getHandlers(): HandlerList {
        return HandlerList()
    }
}