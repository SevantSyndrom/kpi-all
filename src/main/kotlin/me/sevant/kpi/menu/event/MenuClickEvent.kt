package me.sevant.kpi.menu.event

import me.sevant.kpi.menu.Menu
import me.sevant.kpi.menu.Slot
import org.bukkit.entity.Player
import org.bukkit.event.Cancellable
import org.bukkit.event.inventory.ClickType
import org.bukkit.inventory.Inventory

class MenuClickEvent(player: Player, menu: Menu, val inventory: Inventory, val slot: Slot, val clickType: ClickType) :
    MenuEvent(player, menu), Cancellable {

    private var c = false
    override fun setCancelled(bool: Boolean) {
        c = bool
    }

    override fun isCancelled(): Boolean {
        return c
    }
}